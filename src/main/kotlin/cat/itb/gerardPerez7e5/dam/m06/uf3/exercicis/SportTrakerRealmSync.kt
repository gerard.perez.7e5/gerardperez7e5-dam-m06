package cat.itb.gerardPerez7e5.dam.m06.uf3.exercicis

import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import org.bson.types.ObjectId
import java.util.*

private open class ItemSync(
    @PrimaryKey
    var _id: ObjectId = ObjectId.get(),
    var complete: Boolean = false,
    var summary: String = "",
    var owner_id: String = ""): RealmObject {
    constructor() : this(owner_id = "")
    override fun toString() = "Item($_id, $summary)"
}
suspend fun main(){
var realmApp = App.create("MY-App-Id")
val username: String = "d"
    val password: String = "d"

     realmApp = App.create(
    AppConfiguration.Builder("MY-App-Id")
    .log(LogLevel.ALL)
    .build())
realmApp.emailPasswordAuth.registerUser(username, password)
val creds = Credentials.emailPassword(username, password)
realmApp.login(creds)
val user = realmApp.currentUser!!
val config = SyncConfiguration.Builder(user, setOf(ItemSync::class))
    .initialSubscriptions { realm ->
        add(
            realm.query<ItemSync>(),
            "All Items"
        )
//      add(
//          realm.query<Item>("owner_id == $0", realmApp.currentUser!!.id),
//          "User's Items"
//      )
    }
    .waitForInitialRemoteData()
    .build()
println("Realm Path: ${config.path}")
val realm = Realm.open(config)
realm.subscriptions.waitForSynchronization()
realm.writeBlocking {
    val item = ItemSync(owner_id = user.id,
        summary = "Some summary ${Date()}")
    copyToRealm(item)
}}