package cat.itb.gerardPerez7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XmlElement

@kotlinx.serialization.Serializable
@SerialName("activity")
data class Activity(
    @XmlElement(true) val sport: String,
    @XmlElement(true) val duration: String,
)

@kotlinx.serialization.Serializable
@SerialName("sporttraker")
private data class SportTraker(@SerialName("activity") val activity: List<Activity>)


//data/m06/uf1/xml
fun main(){
    var info = thereIsSomeInfo()
}

private fun thereIsSomeInfo():SportTraker?{
return null
}