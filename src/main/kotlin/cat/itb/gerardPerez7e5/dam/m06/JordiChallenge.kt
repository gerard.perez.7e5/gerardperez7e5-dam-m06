package cat.itb.gerardPerez7e5.dam.m06

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val n = 2*scanner.nextInt()-1
    for (x in 1..n*n){
        val a : Int = when{
            x>n-> x-n
            else -> x
        }
    }
}