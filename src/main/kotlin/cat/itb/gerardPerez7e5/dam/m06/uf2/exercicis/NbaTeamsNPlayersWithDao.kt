package cat.itb.gerardPerez7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

object SportTrakersBD : Table(){
    val sport = varchar("sport", 40)
    val duration = varchar("duration", 40)
}
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    Database.connect("jdbc:h2:./ex1.db", "org.h2.Driver")
    transaction {
        SchemaUtils.create(SportTrakersBD)
        println("Which sport have you done?")
        val whatDoYouDo = scanner.nextLine()
        println("How much?")
        val howMuch = scanner.nextLine()

        SportTrakersBD.insert {
            it[sport] = whatDoYouDo
            it[duration] = howMuch
        }

        SportTrakersBD.selectAll().forEach {
            println(it[SportTrakersBD.duration] + " " + it[SportTrakersBD.sport])
        }
    }
}
