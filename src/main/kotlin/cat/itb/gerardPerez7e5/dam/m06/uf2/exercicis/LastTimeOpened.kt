package cat.itb.gerardPerez7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import java.time.LocalTime

object Time : Table() {
    val time = varchar("date", 40)
    val hour = varchar("hour", 40)
}

fun main() {
    Database.connect("jdbc:h2:./ex1.db", "org.h2.Driver")
    transaction {
        SchemaUtils.create(Time)
        Time.insert {
            it[time] = LocalDate.now().toString()
            it[hour] = LocalTime.now().toString()
        }
        Time.selectAll().forEach {
            println(it[Time.time] + " " + it[Time.hour])
        }
    }
}