package cat.itb.gerardPerez7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.util.Scanner


@kotlinx.serialization.Serializable
@SerialName("ingredient")
private data class Ingredient(
    val ammount : String,
    val unit : String,
    val name : String
)

@kotlinx.serialization.Serializable
@SerialName("recipe")
private data class Recipe(
    val dificulty : String,
    @SerialName("name")
    val name: String,
    @SerialName("ingredients")
    val ingredients : List<Ingredient>
)

@kotlinx.serialization.Serializable
@SerialName("recipes")
private data class Recipes(
    @SerialName("recipie")
    val recipes: List<Recipe>
)

fun main(){
//    val scanner = Scanner(System.`in`)
//    val plantajament.txt = scanner.nextLine()
//    RecipiesByIngredient(plantajament.txt)
    val r = Recipes(listOf(Recipe( "5",  "Papates amb llet",  listOf(Ingredient( "125", "grams", "all")))))
    println(XML.encodeToString(r))
}

private fun RecipiesByIngredient(name : String){
    val xml = Recipe::class.java.getResource("/RecipesByIngredient.xml")!!.readText()
    val restaurantIngredient: Recipe = XML.decodeFromString(xml)
    println(restaurantIngredient)
}