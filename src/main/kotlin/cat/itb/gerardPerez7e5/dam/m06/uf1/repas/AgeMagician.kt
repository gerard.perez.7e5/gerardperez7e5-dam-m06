package cat.itb.gerardPerez7e5.dam.m06.uf1.repas


import java.util.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json


@Serializable
data class Data(
    @SerialName("name") val name: String,
    @SerialName("age") val age: Int,
    @SerialName("count") val count: Int,
    @SerialName("country_id") val country_id: String
)


suspend fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val trees: List<Data> = client.get("https://api.agify.io/?name=Alba&country_id=ES").body()
    println(trees[0].name)
}

private fun AgeMagician() {
    println("Ben vingut al AgeMagician. Com et dius?")

    println("Jo crec que tens 28 anys!")
}