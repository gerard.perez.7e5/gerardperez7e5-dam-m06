package cat.itb.gerardPerez7e5.dam.m06.uf3.exercicis

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

import java.util.*

//open class SportTrakerRealm(
//    @PrimaryKey
//    var _id: ObjectId = io.realm.kotlin.types.ObjectId().create,
//    var sport: String = "",
//    var time: Int = 0,
//    var id: String = ""
//) : RealmObject {
//    // Declaring empty contructor
//    constructor() : this(id = "") {}

//    //var doAfter: RealmList<Item>? = realmListOf()
//    override fun toString() = "Item($_id, $sport)"
//}

//val config = RealmConfiguration.Builder(setOf(Item::class))
//    .deleteRealmIfMigrationNeeded()
//    // .directory("customPath")
//    .build()
//println("Realm Path: ${config.path}")
//val realm = Realm.open(config)
//
//realm.writeBlocking {
//    val item = Item(summary = "Some summary ${Date()}"
//        // doAfter = query<Item>().find().take(2).toRealmList()
//    )
//    copyToRealm(item)
//}