package cat.itb.gerardPerez7e5.dam.m06.uf1.repas

import java.nio.file.Path
import kotlin.io.path.*

fun main() {
    val homePathInString = System.getProperty("user.home")
    val number: Int = getNumbersFromDeskctop(homePathInString)
    createFile(homePathInString, number)
}

private fun getNumbersFromDeskctop(homePathInString: String): Int {
    val path = Path("$homePathInString/Escriptori")
    val files: List<Path> = path.listDirectoryEntries()
    val kotlinFiles: List<Path> = path.listDirectoryEntries("*.kt")
    return files.size
}

private fun createFile(homePathInString: String, number: Int) {
    val HomePath = Path(homePathInString)
    val filePath = HomePath.resolve("desktopContents.txt");
    if (filePath.notExists()) {
        filePath.createFile()
    }
    filePath.writeText("$number")
}