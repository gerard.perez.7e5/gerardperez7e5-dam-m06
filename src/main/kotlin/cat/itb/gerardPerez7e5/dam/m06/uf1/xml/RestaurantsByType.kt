package cat.itb.gerardPerez7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML

@kotlinx.serialization.Serializable
@SerialName("restaurants")
private data class Restaurants(@SerialName("restaurant") val restaurant: List<Restaurant>)

fun main() {
    val r = Restaurants(
        listOf(
            Restaurant("tapes", "Bar Pasqual", "Avinguda Diagonal 232", "Joan Martí"),
            Restaurant("restaurant de menú", "Meravelles", "Meridiana 111", "Maria Colomer"),
            Restaurant("tapes", "Tapitapes", "Balmes 2", "Gustaf Michelin")
        )
    )
    // println(XML.encodeToString(r))
    //  val xml = Restaurant::class.java.getResource("/RestaurantsByType.xml")!!.readText()
    //  val restaurant: Restaurant = XML.decodeFromString(xml)
    //  println(restaurant)
    askForARestaurantByType("tapes")
}


private fun askForARestaurantByType(tipo: String) {
    val xml = Restaurant::class.java.getResource("/RestaurantsByType.xml")!!.readText()
    var restaurants: Restaurants = XML.decodeFromString(xml)
    restaurants.restaurant.filter { it.type == tipo }
    println(restaurants)
}