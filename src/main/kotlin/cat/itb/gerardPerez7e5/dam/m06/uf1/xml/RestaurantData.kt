package cat.itb.gerardPerez7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement

//import kotlin.io.path.readText
//import kotlin.io.path.Path

@kotlinx.serialization.Serializable
@SerialName("restaurant")
data class Restaurant(
    val type: String,
    @XmlElement(true) val name: String,
    @XmlElement(true) val address: String,
    @XmlElement(true) val owner: String
)

fun main() {
    // val r = Restaurant("restaurant de menú", name("Meravelles"), address("Meridiana 111"), owner("Maria Colomer"))
    //  println(XML.encodeToString(r))
    val xml = Restaurant::class.java.getResource("/restaurant.xml")!!.readText()
    val restaurantIngredient: Restaurant = XML.decodeFromString(xml)
    println(restaurantIngredient)
}