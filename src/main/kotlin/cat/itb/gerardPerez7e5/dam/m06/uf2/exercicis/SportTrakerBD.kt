package cat.itb.gerardPerez7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import io.ktor.util.date.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.text.DateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

object SportTrakerBD : Table(){
    val sport = varchar("sport", 40)
    val duration = varchar("duration", 40)
}
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    Database.connect("jdbc:h2:./ex1.db", "org.h2.Driver")
    transaction {
        SchemaUtils.create(SportTrakerBD)
        println("Which sport have you done?")
        val whatDoYouDo = scanner.nextLine()
        println("How much?")
        val howMuch = scanner.nextLine()

        SportTrakerBD.insert {
            it[sport] = whatDoYouDo
            it[duration] = howMuch
        }

        SportTrakerBD.selectAll().forEach {
            println(it[SportTrakerBD.duration] + " " + it[SportTrakerBD.sport])
        }
    }
}
